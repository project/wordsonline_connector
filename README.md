WordsOnline Connector (wordsonline_connector)
---------------------
WordsOnline Connector is a Translation Management module.
It connects Drupal to WordsOnline for professional translation services.
It sends your content to WordsOnline and returns translated files.
Users can work within their Drupal environment.

FEATURES
--------
This WordsOnline Connection with Drupal provides:
  * A collaborative translation platform.
  * Review by professional translators.
  * Seamless ordering of translations.
  * Wide range of languages.
  
REQUIREMENTS
------------
  * TMGMT module: http://drupal.org/project/tmgmt
  * PHP 7.4.2+
  * Your WordsOnline API keys.  
    Please contact helpdesk@wordsonline.com.

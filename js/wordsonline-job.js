var root = window.location.origin + window.location.pathname.replace('wordsonline/jobs', 'wordsonline/').replace('wordsonline/wol-order', 'wordsonline/')
function CheckHasFile(isRefresh = false) {
  var num = sessionStorage.getItem('wol-reload');
  if (num == '1') {
    sessionStorage.removeItem('wol-reload');
    return;
  }
  document.getElementById("wol-loader").style.display = "block";
  var url = root + "check-has-file";
  var request = new XMLHttpRequest()
  request.open('POST', url, true)
  request.onload = function () {
    // // Begin accessing JSON data here
    var data = JSON.parse(this.response);
    if (data.status == "success" && data.changed == true) {
      if (isRefresh == true) {
        sessionStorage.setItem('wol-reload', '1');
        Refresh();
      }

    }

    document.getElementById("wol-loader").style.display = "none";
  }
  request.send()
}
function checkQuoted() {
  let page = document.getElementsByName("job_page");
  let name = document.getElementsByName("job_data");
  var params = getAllUrlParams(window.location.href);
  var url = root + "check-quoted" + "?top=" + page[0].value;
  if (params) {
    if (params.skip) {
      url = url + "&skip =" + params.skip;
    }
    if (params.page) {
      url = url + "&page =" + params.page;
    }

  }
  let data = name[0].value;
  if (data != null && data != '') {
    url = url + '&key=' + data;
  }
  document.getElementById("wol-loader").style.display = "block";
  var request = new XMLHttpRequest()
  request.open('POST', url, true)
  request.onload = function () {
    // // Begin accessing JSON data here
    var data = JSON.parse(this.response);
    if (data.status == "success" && data.changed == true) {
      Refresh();
    }

    document.getElementById("wol-loader").style.display = "none";
  }
  request.send()
}
setTimeout(() => {
  CheckHasFile();
}, 5);
function OnRefreshPage() {
  CheckHasFile(true);
  var params = getAllUrlParams(window.location.href);
  let name = document.getElementsByName("job_data");
  let oldSearch = null;
  if (params) {
    if (params.key) {
      oldSearch = params.key;
    }

  }
  let data = name[0].value;
  if (data == '') {
    data = null;
  }
  if (oldSearch != data) {
    Refresh();
    return;
  }
  checkQuoted();
}
document.getElementById("wol-loader").style.display = "none";
const btn = document.querySelector('#btnRefresh');
btn.addEventListener('click', function (event) {
  OnRefreshPage();
});
function reloadPreview(fileName,id){
  let html =document.getElementById(id).innerHTML;
  if(html && html != "" && html != null ){
  return;
  }
  let url = root + "get-file-content?fileName=" + fileName;
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      let data = JSON.parse(this.responseText)
      if (data.status == "success") {
        document.getElementById(id).innerHTML = data.result;
      }
    }
  };
  request.open('GET', url);
  request.send();
}
function downloadJobFile(fileName){
  let url = root + "download-job-file?fileName=" + fileName;
  var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.onprogress = e => console.log(`${parseInt((e.loaded/e.total)*100)}%`)
    xhr.onload = e => {
      const url1 = window.URL.createObjectURL(xhr.response);
      const a = document.createElement('a');
      a.style.display = 'none';
      a.href = url1;
      a.download = fileName; 
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    }
    xhr.send()
}
function reimport(job_id) {
  document.getElementById("wol-loader").style.display = "block";
  var url = root + "reimport?job_id=" + job_id;
  var request = new XMLHttpRequest()
  request.open('POST', url, true)
  request.onload = function () {
    // // Begin accessing JSON data here
    var data = JSON.parse(this.response)

    if (data.status == "success") {
      sessionStorage.setItem('wol-reload', '1');
      window.location.reload();
    }
    document.getElementById("wol-loader").style.display = "block";
  }
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status != 200) {
     location.reload();
    }
  };
  request.send()
}

setTimeout(() => {
  getQuote();
}, 5000);
function getQuote() {
  let link = document.querySelector('.loading-quote');
  // console.log(link);
  if (link) {
    let job_id = link.getAttribute('data-job-id');

    var url = root + "get-quote?job_id=" + job_id;
    var request = new XMLHttpRequest()
    request.open('POST', url, true)
    request.onload = function () {
      // Begin accessing JSON data here
      var data = JSON.parse(this.response)

      if (request.status >= 200 && request.status < 400) {
        if (data.status != 'error') {
          var element = document.getElementsByClassName("loading-quote")[0];
          var element1 = document.getElementsByClassName("order-progress-note")[0];
          element1.classList.remove("d-none");
          var dtpPageCount = document.getElementById("dtpPageCount");
          dtpPageCount.innerHTML = data.data.dtpPageCount + " pages";
          var wordCount = document.getElementById("wordCount");
          wordCount.innerHTML = data.data.wordCount + " words";
          var translationAmount = document.getElementById("translationAmount");
          translationAmount.innerHTML = data.data.quoteSections[0].amount;
          var translationCurrency = document.getElementById("translationCurrency");
          translationCurrency.innerHTML = data.data.quoteSections[0].currency;
          element.classList.add("d-none");

        }
        setTimeout(() => {
          getQuote();
        }, 10000);
      } else {
        setTimeout(() => {
          getQuote();
        }, 5000);
      }
    }

    request.send()
  } else {
    setTimeout(() => {
      getQuote();
    }, 5000);
  }

}
function searchJob() {
  let url = window.location.origin + window.location.pathname;
  let val = document.getElementsByName("job_data");
  let page = document.getElementsByName("job_page");
  if (val) {
    let data = val[0].value;
    if (data != null && data != '') {
      url = url + '?key=' + data;
      if (page) {
        url = url + "&top=" + page[0].value;
      }
    } else {
      if (page) {
        url = url + "?top=" + page[0].value;
      }
    }
  } else {
    if (page) {
      url = url + "?top=" + page[0].value;
    }
  }

  location.href = url;
}
function goPage(num) {
  let url = window.location.origin + window.location.pathname;
  var params = getAllUrlParams(window.location.href);
  url = url + "?page=" + num;

  if (params) {
    if (params.skip) {
      url = url + "&skip =" + params.skip;
    }
    if (params.top) {
      url = url + "&top =" + params.top;
    }
    if (params.key) {
      url = url + "&key =" + params.key;
    }
    if (params.top) {
      url = url + "&top =" + params.top;
    }
  }
  location.href = url;
}
function changePageLimit() {
  let page = document.getElementsByName("job_page");
  let url = window.location.origin + window.location.pathname;
  var params = getAllUrlParams(window.location.href);
  url = url + "?top=" + page[0].value;

  if (params) {
    if (params.skip) {
      url = url + "&skip =" + params.skip;
    }
    if (params.key) {
      url = url + "key =" + params.key;
    }
    // if(params.page){
    //   url=url+"&page ="+params.page;
    // }
  }
  location.href = url;
}
function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      if (typeof paramValue === 'string') {
        paramValue = paramValue.toLowerCase();
      } 

      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {

        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) {
          obj[key] = [];
        }

        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string') {
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }
  return obj;
}
document.getElementsByName("job_data")[0]
  .addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      document.getElementsByName("job-search")[0].click();
    }
  });
function Refresh() {
  let url = window.location.origin + window.location.pathname;
  let page = document.getElementsByName("job_page");
  let name = document.getElementsByName("job_data");
  var params = getAllUrlParams(window.location.href);
  url = url + "?top=" + page[0].value;
  if (params) {
    if (params.skip) {
      url = url + "&skip =" + params.skip;
    }
    if (params.page) {
      url = url + "&page =" + params.page;
    }

  }
  let data = name[0].value;
  if (data != null && data != '') {
    url = url + '&key=' + data;
  }

  if (location.href == url) {
    location.reload();
  } else {
    window.location.href = url.replace( /#/, "" );
  }
}
function onDateChage(obj, timezone) {
  if (obj.value) {
    if (obj.value == null || obj.value == '') return;
    let date = new Date(obj.value);
    let dayOfWeek = date.getDay();
    let isWeekend = (dayOfWeek === 6) || (dayOfWeek === 0);
    if (isWeekend == true) {
      obj.value = null;
      alert("You cann't choose weekend");
    }
  }
}
Array.from(document.querySelectorAll('.use-ajax')).forEach(e => e.addEventListener("click", function (e) {
  var url = e.currentTarget.getAttribute("href");
  Drupal.ajax({ url: url }).execute();
  e.preventDefault();
}))
document.addEventListener("click", function (e) {
  if(e.srcElement.classList.contains('wo-page') == true){
    let curPage = e.srcElement.attributes['data-page'].value;
    if(curPage){
      goPage(curPage);
    }
  }
});

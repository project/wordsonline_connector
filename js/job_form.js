function onContentTypeChange(obj) {

    if (obj) {
        let val = obj.value;
        let data = JSON.parse(obj.getAttribute("service-level-data"));
        let select = document.getElementsByName("settings[service_level]");
        if (select) {
            const count = select[0].options.length;
            for (let i = count - 1; i > 0; i--) {
                select[0].options.remove(i);
            }
            let arr = [];
            for (let index = 0; index < data.length; index++) {
                let element = data[index];
                if (element['contentType'] == val && arr.indexOf(element['serviceName']) < 0) {
                    arr.push(element['serviceName']);
                    var opt = document.createElement('option');
                    opt.value = element['serviceName'];
                    opt.innerHTML = element['serviceName'];
                    select[0].appendChild(opt);
                }
            }
        }
    }
}
function onDateChage(obj) {
    if (obj.value) {
        if (obj.value == null || obj.value == '') return;
        let date = new Date(obj.value);
        let dayOfWeek = date.getDay();
        let isWeekend = (dayOfWeek === 6) || (dayOfWeek === 0);
        if (isWeekend == true) {
            obj.value = null;
            alert("You cann't choose weekend ");
        }
    }
}
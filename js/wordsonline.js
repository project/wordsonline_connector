var root = window.location.origin + window.location.pathname.replace('wordsonline/jobs', 'wordsonline/').replace('wordsonline/wol-order', 'wordsonline/')

function downloadxlf() {
  var a = document.body.appendChild(
    document.createElement("a")
  );
  var filename = "data.xlf";
  var fileType = '.xml';
  var xmls = document.getElementsByClassName("xlf-content");
  for (var i = 0; i < xmls.length; i++) {
    var xmltext = xmls[i].innerHTML;
    var textArea = document.createElement('textarea');
    textArea.innerHTML = xmltext;
    xmltext = textArea.value;
    var blob = new Blob([xmltext], { type: fileType });
    filename = xmls[i].getAttribute("title");;
    var a = document.createElement('a');
    a.download = filename;
    a.href = URL.createObjectURL(blob);
    a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
    a.style.display = "none";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    setTimeout(function () { URL.revokeObjectURL(a.href); }, 1500);
  }

}

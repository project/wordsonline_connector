<?php

namespace Drupal\wordsonline_connector\Common;

use Drupal\Core\Archiver\ArchiverException;
use Drupal\Core\Archiver\Zip;
use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\File\FileSystemInterface;

/**
 * Handle Zip file.
 */
class ZipHandle {
  /**
   * Path of zip file.
   *
   * @var string
   */
  public $zipPath;

  /**
   * Content of zip file.
   *
   * @var array
   */
  public $fileContent;

  /**
   * File name of zip file.
   *
   * @var array
   */
  public $fileNames;

  /**
   * Archiver.
   *
   * @var Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiver;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructor.
   *
   * @param string $zip_path
   *   The path of zip file.
   * @param \Drupal\Core\Archiver\ArchiverManager $archiver
   *   Archiver manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File System.
   */
  public function __construct($zip_path, ArchiverManager $archiver, FileSystemInterface $fileSystem) {
    $this->zipPath = $zip_path;
    $this->fileContent = [];
    $this->fileNames = [];
    $this->archiver = $archiver;
    $this->fileSystem = $fileSystem;
    $this->unzipFile($zip_path);
  }

  /**
   * Unzip file and get contents.
   *
   * @param string $file_path
   *   The path of zip file.
   */
  public function unzipFile($file_path) {
    try {
      $zip = $this->archiver->getInstance(["filepath" => $file_path]);
      if ($zip == NULL) {
        $zip = new Zip($file_path);
      }
      $zip->extract("temporary://wordsonline_connector/");
      $files = $zip->listContents();
      $total = count($files);
      for ($i = 0; $i < $total; $i++) {
        if (!str_ends_with($files[$i], '.xlf')) {
          continue;
        }
        $tempPath = "temporary://wordsonline_connector/$files[$i]";
        $exist = FALSE;
        do {
          $exist = file_exists($tempPath);
        } while ($exist == FALSE);
        $this->fileContent[] = file_get_contents($tempPath);
        $this->fileNames[] = $files[$i];
        //$this->fileSystem->delete($tempPath);
      }

    }
    catch (ArchiverException $exception) {
      watchdog_exception("wordsonline_connector", $exception);
    }
  }

  /**
   * Handle content.
   *
   * @param string $content
   *   The content of zip file.
   *
   * @return string
   *   a return content after handle.
   */
  public function handleContent($content) {
    if ($content == NULL || empty($content)) {
      return $content;
    }
    //$content = html_entity_decode($content);
    $content = $this->replaceBpt($content, 0);
    $content = $this->replaceEpt($content, 0);
    return $content;
  }

  /**
   * Encode content inside bpt tag.
   *
   * @param string $content
   *   The content of zip file.
   * @param string $index
   *   Start index of bpt.
   *
   * @return string
   *   A return content.
   */
  public function replaceBpt($content, $index = 0) {
    $bptIndex = strpos($content, "<bpt", $index);
    if ($bptIndex > 0) {
      $endBptIndex = strpos($content, "</bpt>", $index);
      $subXml = substr($content, $bptIndex, $endBptIndex - $bptIndex + 6);
      $len = strlen($subXml);
      $subLen = $len - 6;
      $sIndex = strpos($subXml, ">");
      $tempAdd = 0;
      if ($subLen > $sIndex + 1) {
        $temp = $subXml;
        for ($i = $sIndex + 1; $i < $subLen; $i++) {
          if ($subXml[$i] == "<") {
            $temp =
                substr($temp, 0, $i + $tempAdd) .
                "&lt;" .
                substr($temp, $i + 1 + $tempAdd);
            $tempAdd = $tempAdd + 3;
          }
          if ($subXml[$i] == ">") {
            $temp =
                substr($temp, 0, $i + $tempAdd) .
                "&gt;" .
                substr($temp, $i + 1 + $tempAdd);
            $tempAdd = $tempAdd + 3;
          }
        }
        $content =
                substr($content, 0, $bptIndex) .
                $temp .
                substr($content, $endBptIndex + 6);
        $index = $endBptIndex + 7;
        return $this->replaceBpt($content, $index);
      }
    }
    return $content;
  }

  /**
   * Encode content inside ept tag.
   *
   * @param string $content
   *   The content of zip file.
   * @param string $index
   *   Start index of ept.
   *
   * @return string
   *   A return content.
   */
  public function replaceEpt($content, $index = 0) {
    $eptIndex = strpos($content, "<ept", $index);
    if ($eptIndex > 0) {
      $endEptIndex = strpos($content, "</ept>", $index);
      $subXml = substr($content, $eptIndex, $endEptIndex - $eptIndex + 6);
      $len = strlen($subXml);
      $subLen = $len - 6;
      $sIndex = strpos($subXml, ">");
      $tempAdd = 0;
      if ($subLen > $sIndex + 1) {
        $temp = $subXml;
        for ($i = $sIndex + 1; $i < $subLen; $i++) {
          if ($subXml[$i] == "<") {
            $temp =
                substr($temp, 0, $i + $tempAdd) .
                "&lt;" .
                substr($temp, $i + 1 + $tempAdd);
            $tempAdd = $tempAdd + 3;
          }
          if ($subXml[$i] == ">") {
            $temp =
                substr($temp, 0, $i + $tempAdd) .
                "&gt;" .
                substr($temp, $i + 1 + $tempAdd);
            $tempAdd = $tempAdd + 3;
          }
        }
        $content =
                substr($content, 0, $eptIndex) .
                $temp .
                substr($content, $endEptIndex + 6);
        $index = $endEptIndex + 7;
        return $this->replaceEpt($content, $index);
      }
    }
    return $content;
  }

}

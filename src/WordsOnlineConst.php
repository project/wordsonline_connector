<?php

namespace Drupal\wordsonline_connector;

/**
 * All constant string.
 */
class WordsOnlineConst {
  const API_URL = "https://api.wordsonline.com/v5/";
  const JOB_NEW = "NEW";
  const JOB_CREATED = "CREATED";
  const JOB_ORDERED = "ORDERED";
  const JOB_QUOTED = "QUOTED";
  const JOB_CANCELLED = "CANCELLED";
  const JOB_DELIVERED = "DELIVERED";
  const JOB_IMPORTED = "IMPORTED";
  const JOB_FINISHED = "FINISHED";
  const JOB_AUTOMATION_FAILED = "AUTOMATION FAILED";
  const CREATE_REQUEST_URL = 'requests';
  const STATE_FINISHED = 5;
  const VIEW_XLF_LINK = 'admin/tmgmt/view-xlf?job_id=';
  const CONFIRM_ORDER_LINK = 'admin/tmgmt/wordsonline/wol-order?job_id=';
  const NOT_AVAILABLE = 'N/A';
  const GET_PROJECT_URL = 'Projects?projectId=';
  const JOB_TABLE = 'wordsonline_connector_jobs';
  const SELECT_CONFIG = 'SELECT  * FROM wordsonline_connector_configs LIMIT 1';
  const SEARCH_PLACEHOLDER = 'Search by request name, language, request id, project id.';
  const HELPDESK_MAIL = 'helpdesk@wordsonline.com';

}

/**
 * Common message for module.
 */
class WordsOnlineMessage {
  const NEW_ORDER_CREATED = "New order was created!";
  const FAILED_TO_CREATE_NEW_ORDER = "Failed to create new order!";
  const UNABLE_TO_CONNECT_SERVICES = 'Unable to connect to wordsonline service due to following error: @error';
  const UNABLE_TO_CONNECT_SERVICES_WITH_URL = 'Unable to connect to the wordsonline service due to following error: @error at @url';
  const AUTHEN_SAVED = 'The authentication data has been succesfully saved';
  const AUTHEN_ERROR = 'Username or password is not correct.';

}

/**
 * State of job from api.
 */
class WordsOnlineState {
  const AUTOMATION_FAILED = 'AutomationFailed';
  const QUOTE_SUBMITTED = 'QuoteSubmitted';
  const PAYMENT_FAILED = 'PaymentFailed';
  const PREPARINGQUOTE = 'Preparing Quote';

}

/**
 * Status of job from api.
 */
class WordsOnlineStatus {
  const IMPORTED_STATUS = 'Imported';
  const IMPORT_FAIL_STATUS = 'ImportFailed';
  const FINISHED_STATUS = 'Finished';
  const DELIVERED = 'Delivered';
  const UNPAID = 'In Your Cart';
  const QUOTE_SUBMITTED = 'Quote Submitted';

}

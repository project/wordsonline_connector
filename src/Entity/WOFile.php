<?php

namespace Drupal\wordsonline_connector\Entity;

/**
 * Struct of xlf file data from api result.
 */
class WOFile {

  /**
   * File Name.
   *
   * @var string
   */
  public $fileName;

  /**
   * File content.
   *
   * @var string
   */
  public $content;

  /**
   * Constructor.
   *
   * @param string $name
   *   File name.
   * @param string $content
   *   File content.
   */
  public function __construct($name, $content) {
    $this->fileName = $name;
    $this->content = $content;
  }

}

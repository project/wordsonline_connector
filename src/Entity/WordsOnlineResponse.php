<?php

namespace Drupal\wordsonline_connector\Entity;

/**
 * Struct of a api response.
 */
class WordsOnlineResponse {

  /**
   * Result.
   *
   * @var string
   */
  public $result;

  /**
   * Status.
   *
   * @var string
   */
  public $status;

  /**
   * Code.
   *
   * @var string
   */
  public $code;

  /**
   * Message.
   *
   * @var string
   */
  public $mesage;

  /**
   * Constructor.
   *
   * @param object $ret
   *   Result.
   * @param string $st
   *   Status.
   * @param string $c
   *   Code.
   * @param string $m
   *   Message.
   */
  public function __construct($ret, $st, $c, $m) {
    $this->result = $ret;
    $this->status = $st;
    $this->code = $c;
    $this->message = $m;
  }

}

<?php

namespace Drupal\wordsonline_connector\Entity;

/**
 * Service level for order.
 */
class WOLServiceLevel {

  /**
   * Service Name.
   *
   * @var string
   */
  public $serviceName;

  /**
   * Content type.
   *
   * @var string
   */
  public $contentType;

  /**
   * Constructor.
   *
   * @param string $name
   *   Service Name.
   * @param string $content
   *   Content Type.
   */
  public function __construct($name, $content) {
    $this->serviceName = $name;
    $this->contentType = $content;
  }

}

<?php

namespace Drupal\wordsonline_connector\Entity;

/**
 * Language pair for order.
 */
class WOLLangPair {

  /**
   * Source language.
   *
   * @var string
   */
  public $sourceLanguage;

  /**
   * Target language.
   *
   * @var string
   */
  public $targetLanguage;

  /**
   * Constructor.
   *
   * @param string $source
   *   Source Language.
   * @param string $target
   *   Target Language.
   */
  public function __construct($source, $target) {
    $this->sourceLanguage = $source;
    $this->targetLanguage = $target;
  }

}

<?php

namespace Drupal\wordsonline_connector\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tmgmt\ContinuousTranslatorInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorPluginBase;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\File\FileSystemInterface;
use Psr\Log\LoggerInterface;
use Drupal\tmgmt_file\Format\FormatManager;
use Drupal\file\FileRepository;
use Drupal\wordsonline_connector\WordsOnlineConst;
use Drupal\wordsonline_connector\WordsOnlineMessage;
use Drupal\Core\Archiver\ArchiverManager;

/**
 * WordsOnline translation plugin controller.
 *
 * @TranslatorPlugin(
 *   id = "wordsonline",
 *   label = @Translation("WordsOnline"),
 *   description = @Translation("WordsOnline translator service."),
 *   ui = "Drupal\wordsonline_connector\WordsOnlineTranslatorUi",
 *   logo = "icons/wordsonline-startup.png",
 * )
 */
class WordsOnlineTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface, ContinuousTranslatorInterface {

  use StringTranslationTrait;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The file format manager.
   *
   * @var \Drupal\tmgmt_file\Format\FormatManager
   */
  protected $formatManager;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * Archiver.
   *
   * @var Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiver;

  /**
   * Constructs a WordsOnlineTranslator object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File System.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\tmgmt_file\Format\FormatManager $formatManager
   *   The file format manager.
   * @param \Drupal\file\FileRepository $fileRepository
   *   The file format manager.
   * @param \Drupal\Core\Archiver\ArchiverManager $archiver
   *   Archiver manager.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(ClientInterface $client, Connection $database, Messenger $messenger, FileSystemInterface $file_system, LoggerInterface $logger, FormatManager $formatManager, FileRepository $fileRepository, ArchiverManager $archiver, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
    $this->database = $database;
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
    $this->logger = $logger;
    $this->formatManager = $formatManager;
    $this->fileRepository = $fileRepository;
    $this->archiver = $archiver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $client = $container->get('http_client');
    $database = $container->get('database');
    $messenger = $container->get('messenger');
    $fileSystem = $container->get('file_system');
    $logger = $container->get('logger.factory')
      ->get("wordsonline_connector");
    $manager = $container->get('plugin.manager.tmgmt_file.format');
    $fileRepository = $container->get('file.repository');
    $archiver = $container->get('plugin.manager.archiver');
    return new static(
      $client,
      $database,
      $messenger,
      $fileSystem,
      $logger,
      $manager,
      $fileRepository,
      $archiver,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param array $job_items
   *   List job item.
   */
  public function requestJobItemsTranslation(array $job_items) {

  }

  /**
   * {@inheritdoc}
   *
   * @param Drupal\tmgmt\JobInterface $job
   *   Job.
   */
  public function requestTranslation(JobInterface $job) {
    $translator = $job->getTranslator();
    $translator->setSetting('xliff_processing', TRUE);
    $translator->save();
    try {
      $source_lang = $job->getSourceLangcode();
      $target_lang = $job->getTargetLangcode();
      $request_name = $job->getSetting('request_name');
      $projectId = $job->getSetting('project_key');
      if ($projectId == NULL) {
        $projectId = '';
      }
      $results = $this->database->query('SELECT  code,wol_code FROM wordsonline_connector_support_languges')->fetchAll();
      foreach ($results as $row) {
        if ($row->code == $source_lang) {
          $source_lang = $row->wol_code;
        }
        if ($row->code == $target_lang) {
          $target_lang = $row->wol_code;
        }
      }
      $params['request_name'] = $request_name;
      $params['projectId'] = $projectId;
      $params['sourceLanguage'] = $source_lang;
      $params['targetLanguages'] = $target_lang;
      $params['contentTypeId'] = $job->getSetting('content_type');
      $params['serviceLevel'] = $job->getSetting('service_level');
      $auto_approve_quote = $job->getSetting('auto_approve_quote');
      $params['auto_approve_quote'] = $auto_approve_quote == TRUE || $auto_approve_quote == 1 ? 'true' : 'false';
      $xliff = $this->formatManager->createInstance('xlf', ['target' => 'source']);
      $xliff_content = $xliff->export($job);
      $file_zip = "Job{$job->id()}.zip";
      $job_items = array_values($job->getItems());
      $zip_files = [];
      $path = "public://";
      foreach ($job_items as $job_item) {
        $job_item_id = $job_item->id();
        $xliff = $this->formatManager->createInstance('xlf', ['target' => 'source']);
        $conditions = ['tjiid' => ['value' => $job_item_id]];
        $xliff_content = $xliff->export($job, $conditions);
        // Build a file name.
        $file_name = "Job_{$job->id()}_{$job_item_id}.xlf";
        array_push($zip_files, "public://Job_{$job->id()}_{$job_item_id}.xlf");
        if ($this->fileSystem->prepareDirectory(
            $path,
            FileSystemInterface::CREATE_DIRECTORY
          )
        ) {
          $version = (float) (str_replace("-dev", "", \Drupal::VERSION));
          if ($version >= 9.3) {
            $this->fileRepository->writeData(
              $xliff_content,
              $path . $file_name
            );
          }
          else {
            file_save_data(
              $xliff_content,
              $path . $file_name
            );
          }
        }
        $fields["job_id"] = $job->id();
        $fields["job_item_id"] = $job_item_id;
        $fields["file_name"] = $file_name;
        $fields["file_path"] = $path . $file_name;
        $fields["source_language"] = $source_lang;
        $fields["target_language"] = $target_lang;
        $f_ret = $this->database
          ->query("SELECT  * FROM 
            wordsonline_connector_job_files 
            where job_id = :job_id and job_item_id = :job_item_id LIMIT 1",
            [
              ':job_id' => $job->id(),
              ':job_item_id' => $job_item_id,
            ]
          )
          ->fetchAssoc();
        if ($f_ret != NULL) {
          $this->database
            ->query("Delete FROM wordsonline_connector_job_files 
              where job_id = :job_id and job_item_id = :job_item_id LIMIT 1",
              [
                ':job_id' => $job->id(),
                ':job_item_id' => $job_item_id,
              ]
            )
            ->fetchAssoc();
        }
        $this->database->insert('wordsonline_connector_job_files')
          ->fields($fields)->execute();
        if ($job_item->getJob()->isContinuous()) {
          $job_item->active();
        }
      }
      $this->createZip($zip_files, $path . $file_zip);
      $f_ret = $this->database
        ->query("SELECT  * FROM 
          wordsonline_connector_jobs 
          where job_id = :job_id  and status = :s LIMIT 1",
          [
            ':job_id' => $job->id(),
            ':s' => WordsOnlineConst::JOB_NEW,
          ]
        )
        ->fetchAssoc();
      if ($f_ret == NULL) {
        $orders["job_id"] = $job->id();
        $orders["file_guid"] = '';
        $orders["request_guid"] = '';
        $orders["request_name"] = $request_name;
        $orders["project_guid"] = $projectId;
        $orders["status"] = WordsOnlineConst::JOB_NEW;
        $this->database->insert(WordsOnlineConst::JOB_TABLE)
          ->fields($orders)->execute();
      }
      $params['job_id'] = $job->id();
      $results = $this->database->query('SELECT  * FROM wordsonline_connector_configs LIMIT 1')->fetchAssoc();
      $is_created = FALSE;
      if ($results != NULL) {
        $auth["username"] = $results["username"];
        $auth["password"] = $results["password"];
        $auth["scope"] = $results["scope"];
        $auth["grant_type"] = $results["grant_type"];
        $response = wordsonline_connector_get_token('POST', $auth);
        $response = json_decode($response, TRUE);
        $token = $response['access_token'];
        $files = $this->uploadFiles($path . $file_zip, $token);
        if ($files["status"] >= 0) {
          $params['fileList'] = $files["result"];
          $is_created = $this->createOrder(WordsOnlineConst::CREATE_REQUEST_URL, 'POST', $params, $token);
        }
      }
      else {
        $this->messenger->addError(
          $this->t(
            'Cannot find WordsOnline provider settings. 
            Please contact <a href="mailto:@url">@url<a/> 
            to obtain WordsOnline provider settings.',
            [
              "@url" => WordsOnlineConst::HELPDESK_MAIL,
            ]
          )
        );
      }
      if ($is_created == FALSE) {
        $this->fileSystem->delete($path . $file_zip);
        return;
      }
      $submit_message = 'The translation job has been submitted. Please go to <a href="' .
        base_path() .
        'admin/tmgmt/wordsonline/jobs' .
        '"> WordsOnline Translation Request</a> to see your quote.';
      $job->submitted($submit_message);
      $this->fileSystem->delete($path . $file_zip);
    }
    catch (TMGMTException $e) {
      $job->rejected('Job has been rejected with following error: @error', ['@error' => $e->getMessage()], 'error');
    }
  }

  /**
   * Creates a compressed zip file.
   *
   * @param array $files
   *   The path file array.
   * @param string $destination
   *   The target zip path.
   * @param bool $overwrite
   *   Is Overite.
   *
   * @return bool
   *   Check is succeess.
   */
  private function createZip(array $files = [], $destination = '', $overwrite = FALSE) {
    if (file_exists($destination) && !$overwrite) {
      $overwrite = TRUE;
    }
    $valid_files = [];
    if (is_array($files)) {
      foreach ($files as $file) {
        if (file_exists($file)) {
          $valid_files[] = $file;
        }
      }
    }
    if (count($valid_files)) {
      $zip = new \ZipArchive();
      $destination = $this->fileSystem->realpath($destination);
      if ($zip->open($destination, $overwrite ? \ZipArchive::OVERWRITE : \ZipArchive::CREATE) !== TRUE) {
        return FALSE;
      }
      foreach ($valid_files as $file) {
        $realFile = $this->fileSystem->realpath($file);
        $zip->addFile($realFile, str_replace("public://", "", $file));
      }
      $zip->close();
      return file_exists($destination);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get quote.
   *
   * @param string $file_path
   *   Path of file.
   * @param string $token
   *   Token.
   *
   * @return string
   *   Receive data.
   */
  public function uploadFiles($file_path, $token) {
    $url = WordsOnlineConst::API_URL . 'Files';
    $options = [];
    $options['headers'] = [
      'Authorization' => 'Bearer ' . $token,
      'Referer' => 'ClientAPI',
    ];
    $options['multipart'] = [
        [
          'name' => 'Source',
          'filename' => $file_path,
          'contents' => fopen($file_path, 'r'),
          'headers' => [
            'Content-Type' => '<Content-type header>',
          ],
        ]
      ];
    $options['timeout'] = 3600;
    $result = $this->client->post($url, $options);
    if ($result->getStatusCode() == 200) {
      $data = json_decode($result->getBody()->getContents(), TRUE);
      return $data;
    }
    else {
      return new WordsOnlineResponse(NULL, -1, "B0001", "error");
    }
  }

  /**
   * Create WordsOnline order.
   *
   * @param string $path
   *   The path of api.
   * @param string $method
   *   The method is used.
   * @param array $params
   *   List params.
   * @param string $token
   *   The token for authorization.
   *
   * @return bool
   *   Check is succeess.
   */
  public function createOrder($path, $method = 'POST', array $params = [], $token = NULL) {
    $options = [];
    $job_id = $params['job_id'];
    $url = WordsOnlineConst::API_URL . $path;
    try {
      $response = $this->client->post(
        $url,
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
            'Referer' => 'ClientAPI',
          ],
          'json' => [
            'fileList' => $params['fileList'],
            'requestName' => $params['request_name'],
            'projectId' => $params['projectId'],
            'sourceLanguage' => $params['sourceLanguage'],
            'targetLanguages' => [
              $params['targetLanguages'],
            ],
            'contentTypeId' => $params['contentTypeId'],
            'serviceLevel' => $params['serviceLevel'],
            'description' => 'Request from drupal',
            "clientRequestId" => "Default ID",
            "isAutoApprove" => $params['auto_approve_quote'],
          ],
          'timeout' => 3600,
        ]
      );
    }
    catch (RequestException $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addError(WordsOnlineMessage::FAILED_TO_CREATE_NEW_ORDER);
      return FALSE;
    }
    $received_data = $response->getBody()->getContents();
    $err_msg = WordsOnlineMessage::FAILED_TO_CREATE_NEW_ORDER;
    if ($response->getStatusCode() != 200) {
      $this->messenger->addError(WordsOnlineMessage::FAILED_TO_CREATE_NEW_ORDER);
      return FALSE;
    }
    $orderResponse = json_decode($received_data, TRUE);
    if ($orderResponse["status"] == 1) {
      $this->database->update(WordsOnlineConst::JOB_TABLE)
        ->condition('job_id', $job_id)
        ->fields(
          [
            'status' => WordsOnlineConst::JOB_CREATED,
            'request_guid' => $orderResponse["result"],
          ]
        )
        ->execute();
      $this->messenger->addMessage(WordsOnlineMessage::NEW_ORDER_CREATED);
      return TRUE;
    }
    else {
      if ($orderResponse != NULL && $orderResponse['message'] != NULL) {
        $err_msg = $err_msg . "\r\n" . $orderResponse['message'];
      }
      $this->messenger->addError($err_msg);
    }
    return FALSE;
  }

}

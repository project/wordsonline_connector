<?php

namespace Drupal\wordsonline_connector\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileUrlGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Form to view delivery file.
 */
class ViewXlfForm extends FormBase {

  use StringTranslationTrait;

  /**
   * List Module.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $listModule;

  /**
   * File Url Generator.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $listModule
   *   List Module.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   File Url Generator.
   */
  public function __construct(ModuleExtensionList $listModule, FileUrlGenerator $fileUrlGenerator) {
    $this->listModule = $listModule;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $listModule = $container->get('extension.list.module');
    $fileUrlGenerator = $container->get('file_url_generator');
    return new static(
      $listModule,
      $fileUrlGenerator
    );
  }

  /**
   * Get Id of form.
   *
   * @return string
   *   Return form id.
   */
  public function getFormId() {
    return 'wol_view_xlf_form';
  }

  /**
   * Build form ui.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $files
   *   File array.
   * @param string $fileName
   *   File Name.
   * 
   * @return array
   *   Return a array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, array $files = [], string $fileName ="") {
    $form['#prefix'] = "<div>";
    $form['#suffix'] = "</div>";
    $npath = $this->listModule->getPath('wordsonline_connector');
    $downloadIcon = $this->fileUrlGenerator->generateAbsoluteString($npath . '/icons/file-download.svg');
    $markup = new FormattableMarkup("<p><a class='button' href='#'  onclick='downloadJobFile(@fileName)'><img src='@file' width ='18'/>Download</a></p><hr/>", ['@fileName' => "'" . basename($fileName) . "'", '@file' => $downloadIcon]);
    $form['info_prefix'] = [
      '#markup' => $markup,
    ];

    foreach ($files as $fi) {
      $content_id = uniqid();
      $fileName = basename($fi->fileName);
      $form[$fileName] = [
        '#type' => 'details',
        '#title' => $fileName ,
        '#group' => 'advanced',
        '#attributes' => ['onclick' => "reloadPreview ('" . $fi->fileName . "','" . $content_id . "');"],
      ];
      $markup = new FormattableMarkup(
        "<div><pre lang='xml' class='xlf-content' title='@title' id='@id'>@xml</pre></div>",
        [
          '@title' => $fileName,
          '@id' => $content_id,
          '@xml' => $fi->content,
        ]
      );
      $form[$fileName][$fileName] = [
        '#markup' => $markup,
      ];
    }
    $form['#attached']['library'][] = 'wordsonline_connector/wordsonline';
    return $form;
  }

  /**
   * Validate the form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Submit the form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}

<?php

namespace Drupal\wordsonline_connector\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Archiver\ArchiverException;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\Messenger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\File\FileSystemInterface;
use Psr\Log\LoggerInterface;

/**
 * Form to update version.
 */
class UpdateForm extends FormBase {

  use StringTranslationTrait;
  const FILE_EXISTS_REPLACE = 1;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Archiver.
   *
   * @var Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiver;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger.
   * @param \Drupal\Core\Archiver\ArchiverManager $archiver
   *   Archiver manager.
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File System.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(Connection $database, Messenger $messenger, ArchiverManager $archiver, ModuleHandler $moduleHandler, FileSystemInterface $file_system, LoggerInterface $logger) {
    $this->database = $database;
    $this->messenger = $messenger;
    $this->archiver = $archiver;
    $this->moduleHandler = $moduleHandler;
    $this->fileSystem = $file_system;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $database = $container->get('database');
    $messenger = $container->get('messenger');
    $archiver = $container->get('plugin.manager.archiver');
    $moduleHandler = $container->get('module_handler');
    $fileSystem = $container->get('file_system');
    $logger = $container->get('logger.factory')
      ->get("wordsonline_connector");
    return new static(
      $database,
      $messenger,
      $archiver,
      $moduleHandler,
      $fileSystem,
      $logger
    );
  }

  /**
   * Get Id of form.
   *
   * @return string
   *   Return form id.
   */
  public function getFormId() {
    return 'wol_update_form';
  }

  /**
   * Build the form UI.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return array
   *   Return form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['new_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Update file'),
      '#description' => $this->t('Allowed extensions: zip'),
      '#description_display' => 'after',
      '#required' => TRUE,
      '#upload_location' => 'public://wordsonline_upload',
      '#upload_validators' => [
        'file_validate_extensions' => ['zip'],
      ],
    ];
    $form['#attributes']['enctype'] = 'multipart/form-data';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#name' => 'update',
    ];
    $form_state->disableCache();
    return $form;
  }

  /**
   * Remove old version zip files.
   *
   * @param string $fileName
   *   Return file name.
   */
  public function remove($fileName) {
    try {
      $absolute_path = $this->fileSystem->realpath('public://wordsonline_upload');
      $files = glob("{$absolute_path}/*");
      foreach ($files as $file) {
        if (is_file($file)) {
          $public_path = str_replace($absolute_path, 'public://wordsonline_upload', $file);
          if ($public_path == $fileName) {
            continue;
          }
          unlink($file);
          $query = $this->database->delete('file_managed');
          $query->condition('uri', $public_path);
          $query->execute();
        }
      }
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * Update version by replace files.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function update(array $form, FormStateInterface $form_state) {
    try {
      $field = $form_state->getUserInput();
      $file = $field['new_file'];
      $fids = $file['fids'];
      $f_ret = $this->database->query("SELECT  * FROM file_managed where fid =:fid LIMIT 1", [':fid' => $fids])->fetchAssoc();
      if ($f_ret != NULL) {
        $file_path = $f_ret['uri'];
        $this->remove($file_path);
        $module_path = $this->fileSystem->realpath($this->moduleHandler->getModule('wordsonline_connector')->getPath());
        $zip = $this->archiver->getInstance(['filepath' => $file_path]);
        try {
          $zip->extract($module_path);
        }
        catch (ArchiverException $exception) {
          watchdog_exception('wordsonline_connector', $exception);
          $this->messenger->addMessage("Update error");
        }
      }
      $url = new Url('wordsonline_connector.wol_update', [], []);
      $url->setAbsolute();
      $response = new RedirectResponse($url->toString());
      $response->send();
      $this->messenger->addMessage("Update successful");

    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
      $url = new Url('wordsonline_connector.wol_update', [], []);
      $url->setAbsolute();
      $response = new RedirectResponse($url->toString());
      $response->send();
      $this->messenger->addMessage("Update error");
      return;
    }

  }

  /**
   * Validate the form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Submit the form.
   *
   * @param array $form
   *   Form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->update($form, $form_state);
  }

}

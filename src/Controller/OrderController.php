<?php

namespace Drupal\wordsonline_connector\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\wordsonline_connector\WordsOnlineConst;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobInterface;
use Drupal\wordsonline_connector\Entity\WOFile;
use Drupal\wordsonline_connector\Common\ZipHandle;
use Drupal\wordsonline_connector\WordsOnlineState;
use Drupal\wordsonline_connector\WordsOnlineStatus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\File\FileSystemInterface;
use Drupal\tmgmt_file\Format\FormatManager;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\wordsonline_connector\Plugin\tmgmt\Translator\WordsOnlineTranslator;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Controller for handle order.
 */
class OrderController extends ControllerBase {
  use StringTranslationTrait;

  /**
   * A translation job that has been submitted to the translator.
   *
   * Translator plugins are responsible for setting this state in their
   * implementation of
   * TranslatorPluginControllerInterface::requestTranslation().
   */
  const STATE_ACTIVE = 1;

  /**
   * A continuous translation job.
   *
   * A default state for all continuous jobs.
   */
  const STATE_CONTINUOUS = 6;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Archiver.
   *
   * @var Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiver;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file format manager.
   *
   * @var \Drupal\tmgmt_file\Format\FormatManager
   */
  protected $formatManager;

  /**
   * The tmgmt job entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $jobEntityQuery;

  /**
   * Constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   Http client.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger.
   * @param \Drupal\Core\Archiver\ArchiverManager $archiver
   *   Archiver manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File System.
   * @param \Drupal\tmgmt_file\Format\FormatManager $formatManager
   *   The file format manager.
   * @param \Drupal\Core\Entity\Query\QueryInterface $jobEntityQuery
   *   The tmgmt job entity query.
   */
  public function __construct(ClientInterface $client, Connection $database, Messenger $messenger, ArchiverManager $archiver, FileSystemInterface $fileSystem, FormatManager $formatManager, QueryInterface $jobEntityQuery) {
    $this->client = $client;
    $this->database = $database;
    $this->messenger = $messenger;
    $this->archiver = $archiver;
    $this->fileSystem = $fileSystem;
    $this->formatManager = $formatManager;
    $this->jobEntityQuery = $jobEntityQuery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $client = $container->get('http_client');
    $database = $container->get('database');
    $messenger = $container->get('messenger');
    $archiver = $container->get('plugin.manager.archiver');
    $fileSystem = $container->get('file_system');
    $manager = $container->get('plugin.manager.tmgmt_file.format');
    $jobEntityQuery = $container->get('entity_type.manager')
      ->getStorage("tmgmt_job")->getQuery('AND');
    return new static(
        $client,
        $database,
        $messenger,
        $archiver,
        $fileSystem,
        $manager,
        $jobEntityQuery
    );
  }

  /**
   * Open confirm form.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   */
  public function openMyModal(Request $request) {
    if ($request->query->get("job_id")) {
      $job_ids = $request->query->get("job_id");
    }
    else {
      // First get all translator IDs that use wordsonline.
      $wordsonline_translator_ids = array_keys(
        array_filter($translators, function (TranslatorInterface $translator) {
          $translator_plugin = $translator->getPlugin();
          return $translator_plugin instanceof WordsOnlineTranslator;
        }
        )
      );
      // Then fetch all active or continuous jobs using those translators.
      $job_ids = $this->jobEntityQuery
        ->condition("translator", $wordsonline_translator_ids, "IN")
        ->condition(
        "state",
        [
          OrderController::STATE_ACTIVE,
          OrderController::STATE_CONTINUOUS,
        ],
        "IN"
        )
        ->execute();
    }
    $allowSubmmit = "true";
    if ($request->query->get("allowSubmmit")) {
      $allowSubmmit = $request->query->get("allowSubmmit");
    }
    $modal_form = $this->formBuilder()->getForm(
            '\Drupal\wordsonline_connector\Form\OrderConfirmForm',
            $allowSubmmit,
            $job_ids
    );
    $render["form"] = $modal_form;
    return $render;
  }

  /**
   * Check quote status.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A json response.
   */
  public function checkQuoted(Request $request) {
    $has_change = FALSE;
    $key = NULL;
    if ($request->query->get("key")) {
      $key = $request->query->get("key");
    }
    $top = "10";
    $skip = 0;
    if ($request->query->get("page")) {
      $skip = $request->query->get("page");
    }
    if ($request->query->get("top")) {
      $top = $request->query->get("top");
    }
    $list = $this->getDataList($key, $skip, $top);
    if ($list != NULL) {
      $data = $list->list;
      $count = $list->count;
      if ($count > 0) {
        foreach ($data as $row) {
          $results = $this->database
            ->query(
                "SELECT request_guid,job_id,status 
                FROM wordsonline_connector_jobs 
                WHERE request_guid =:request_guid  LIMIT 1 ",
                [":request_guid" => (string) $row->requestGuid]
            )
            ->fetchAssoc();
          if ($results != NULL) {
            $job_id = $results["job_id"];
            $status = $results["status"];
            if ($row->state == WordsOnlineState::AUTOMATION_FAILED
              && $status != WordsOnlineConst::JOB_AUTOMATION_FAILED
            ) {
              $this->database
                ->update(WordsOnlineConst::JOB_TABLE)
                ->condition("job_id", $job_id)
                ->fields(
                  [
                    "status" => WordsOnlineConst::JOB_AUTOMATION_FAILED,
                  ]
                )
                ->execute();
              $has_change = TRUE;
              continue;
            }
            if ($row->state == WordsOnlineState::QUOTE_SUBMITTED
              && ($row->status == WordsOnlineStatus::UNPAID
              || $row->status == WordsOnlineStatus::QUOTE_SUBMITTED)
              && $status != WordsOnlineConst::JOB_QUOTED
            ) {
              $this->database
                ->update(WordsOnlineConst::JOB_TABLE)
                ->condition("job_id", $job_id)
                ->fields(
                  [
                    "status" => WordsOnlineConst::JOB_QUOTED,
                  ]
                )
                ->execute();
              $has_change = TRUE;
              continue;
            }
            if ($row->status == WordsOnlineStatus::DELIVERED
              || $row->status == WordsOnlineStatus::IMPORT_FAIL_STATUS
              || $row->status == "Import Failed"
            ) {
              if ($status != WordsOnlineConst::JOB_DELIVERED
                && $status != WordsOnlineConst::JOB_IMPORTED
                && $status != WordsOnlineConst::JOB_FINISHED
              ) {
                $has_change = TRUE;
                continue;
              }
            }
            $job = Job::load($job_id);
            if ($job) {
              $state = $job->getState();
              if ($state == WordsOnlineConst::STATE_FINISHED
                && $row->status != WordsOnlineStatus::FINISHED_STATUS
              ) {
                $has_change = TRUE;
                continue;
              }
            }
          }
        }
      }
    }
    return new JsonResponse(
      [
        "status" => "success",
        "changed" => $has_change,
      ]
    );
  }

  /**
   * Display list wordsonline job.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   */
  public function jobList(Request $request) {
    $key = NULL;
    if ($request->query->get("key")) {
      $key = $request->query->get("key");
    }
    $top = "10";
    $skip = 0;
    if ($request->query->get("page")) {
      $skip = $request->query->get("page");
    }
    if ($request->query->get("top")) {
      $top = $request->query->get("top");
    }
    $header = [
      "id" => $this->t("Id"),
      "label" => $this->t("Label"),
      "project_name" => $this->t("Project Name"),
      "request_id" => $this->t("Request Id"),
      "request_name" => $this->t("Request Name"),
      "lang_name" => $this->t("Language"),
      "due_date" => $this->t("Due Date"),
      "status" => $this->t("Status"),
      "opt" => $this->t("Operations"),
    ];
    $form["#prefix"] =
        "<div><a id='btnRefresh'  href='#' 
         class='wol-btn btn-layout'>Refresh Page</a>
         </div> <div id='wol-loader'></div>";
    $list = $this->getDataList($key, $skip, $top);
    $rows = [];
    $count = 0;
    $pageCount = 0;
    if ($list != NULL) {
      $data = $list->list;
      $count = $list->count;
      $numOfPage = is_numeric($top) ? (int) $top : 10;
      if ($count % $numOfPage > 0) {
        $pageCount = $count / $numOfPage + 1;
      }
      else {
        $pageCount = $count / $numOfPage;
      }
      $configs = $this->database
        ->query(WordsOnlineConst::SELECT_CONFIG)
        ->fetchAssoc();
      if ($configs != NULL) {
        $auth["username"] = $configs["username"];
        $auth["password"] = $configs["password"];
        $auth["scope"] = $configs["scope"];
        $auth["grant_type"] = $configs["grant_type"];
      }
      $results = $this->database
        ->query(
          "SELECT request_guid,job_id,status 
           FROM wordsonline_connector_jobs 
           order by job_id desc "
        )
        ->fetchAll();

      foreach ($data as $row) {
        $job_id = "";
        $operation = "";
        $due_date = "";
        $label = "";
        $status = WordsOnlineConst::NOT_AVAILABLE;
        foreach ($results as $res) {
          if ($res->request_guid == $row->requestGuid) {
            $job_id = $res->job_id;
            $job = Job::load($job_id);
            if ($job) {
              $label = html_entity_decode($job->label());
              $status = $row->status;
              $due_date = $job->getSetting("due_date");
              $state = $job->getState();
              $jobItems = array_values($job->getItems());
              if ($row->status == WordsOnlineStatus::IMPORTED_STATUS && $res->status == WordsOnlineConst::JOB_DELIVERED) {
                $this->database
                  ->update(WordsOnlineConst::JOB_TABLE)
                  ->condition("job_id", $job_id)
                  ->fields(
                  [
                    "status" => WordsOnlineConst::JOB_IMPORTED,
                  ]
                  )
                  ->execute();
                $res->status = WordsOnlineConst::JOB_IMPORTED;
              }
              if ($state == WordsOnlineConst::STATE_FINISHED) {
                if ($row->status != WordsOnlineStatus::FINISHED_STATUS) {
                  $response = wordsonline_connector_get_token("POST", $auth);
                  $response = json_decode($response, TRUE);
                  $token = $response["access_token"];
                  wordsonline_connector_request_action(
                    $row->requestGuid,
                    WordsOnlineStatus::FINISHED_STATUS,
                    "",
                    $token
                  );
                  $status = WordsOnlineStatus::FINISHED_STATUS;
                }
                if (count($jobItems) > 1) {
                  $url = base_path() .
                    "admin/tmgmt/jobs/" .
                    $job_id;
                }
                else {
                  $url = base_path() .
                    "admin/tmgmt/items/" .
                    $jobItems[0]->id() .
                    "?destination=" .
                    base_path() .
                    "admin/tmgmt/jobs/" .
                    $job_id;
                }
                $edit = $url;
                $operation = $this->t(
                  "<a href=':url' target='_blank' 
                  class='btn'  title='View Job'>View Job</a>",
                  [
                    ":url" => $edit,
                  ]
                );
                $this->database
                  ->update(WordsOnlineConst::JOB_TABLE)
                  ->condition("job_id", $job_id)
                  ->fields(
                    [
                      "status" => WordsOnlineConst::JOB_FINISHED,
                    ]
                  )
                  ->execute();
                break;
              }
              if ($row->state == WordsOnlineState::AUTOMATION_FAILED) {
                $this->database
                  ->update(WordsOnlineConst::JOB_TABLE)
                  ->condition("job_id", $job_id)
                  ->fields(
                    [
                      "status" => WordsOnlineConst::JOB_AUTOMATION_FAILED,
                    ]
                  )
                  ->execute();
                $status = WordsOnlineConst::JOB_AUTOMATION_FAILED;
                break;
              }
              if ($row->status == "In Progress") {
                $edit = base_path() .
                  WordsOnlineConst::CONFIRM_ORDER_LINK .
                  $job_id .
                  "&allowSubmmit=false";
                $edit_link = $this->t(
                  "<a href=':url' class='use-ajax btn'  
                   data-dialog-type='modal' title='View Quote'
                   >View Quote</a>",
                  [
                    ":url" => $edit,
                  ]
                );
                $operation = $edit_link;
                $this->database
                  ->update(WordsOnlineConst::JOB_TABLE)
                  ->condition("job_id", $job_id)
                  ->fields(
                    [
                      "status" => WordsOnlineConst::JOB_QUOTED,
                    ]
                  )
                  ->execute();
                break;
              }
              if ($row->state == WordsOnlineState::QUOTE_SUBMITTED
                && ($row->status == WordsOnlineStatus::UNPAID
                || $row->status == WordsOnlineStatus::QUOTE_SUBMITTED)
                ) {
                $edit = base_path() .
                  WordsOnlineConst::CONFIRM_ORDER_LINK .
                  $job_id;
                $operation = $this->t(
                  "<a href=':url' class='use-ajax btn'  
                  data-dialog-type='modal' title='View Quote'
                  >View Quote</a>",
                  [
                    ":url" => $edit,
                  ]
                );
                $this->database
                  ->update(WordsOnlineConst::JOB_TABLE)
                  ->condition("job_id", $job_id)
                  ->fields(
                    [
                      "status" => WordsOnlineConst::JOB_QUOTED,
                    ]
                  )
                  ->execute();
                break;
              }
              if ($row->state == WordsOnlineState::PAYMENT_FAILED
                  && ($row->status == WordsOnlineStatus::UNPAID
                  || $row->status == WordsOnlineStatus::QUOTE_SUBMITTED)
                  ) {
                $edit = base_path() .
                    WordsOnlineConst::CONFIRM_ORDER_LINK .
                    $job_id;
                $operation = $this->t(
                  "<a href=':url' class='use-ajax btn'  
                   data-dialog-type='modal' title='View Quote'
                   >View Quote</a>",
                    [
                      ":url" => $edit,
                    ]
                );
                break;
              }
              if ($res->status == WordsOnlineConst::JOB_IMPORTED) {
                if ($row->status != WordsOnlineStatus::IMPORTED_STATUS) {
                  $response = wordsonline_connector_get_token("POST", $auth);
                  $response = json_decode($response, TRUE);
                  $token = $response["access_token"];
                  wordsonline_connector_request_action(
                    $row->requestGuid,
                    WordsOnlineStatus::IMPORTED_STATUS,
                    "",
                    $token
                  );
                  $status = WordsOnlineStatus::IMPORTED_STATUS;
                }
                $edit_link = "";
                if (count($jobItems) > 1) {
                  $url = base_path() .
                    "admin/tmgmt/jobs/" .
                    $job_id;
                }
                else {
                  $url = base_path() .
                    "admin/tmgmt/items/" .
                    $jobItems[0]->id() .
                    "?destination=" .
                    base_path() .
                    "admin/tmgmt/jobs/" .
                    $job_id;
                }
                $edit = $url;
                $down = base_path() .
                  WordsOnlineConst::VIEW_XLF_LINK .
                  $job_id;
                $operation = $this->t(
                    "<a href=':url_edit' target='_blank' 
                    class='btn' title='Review'>Review</a>  
                    <a href=':url' class='use-ajax btn' 
                    data-dialog-type='modal'
                    title='download'>Download</a>  
                    <a  class='btn' onclick='reimport(@job_id
                    )'  title='Reimport'>Reimport</a>",
                    [
                      ":url_edit" => $edit,
                      ":url" => $down,
                      '@job_id' => $job_id,
                    ]
                  );
                break;
              }
              if ($row->status == WordsOnlineStatus::DELIVERED
                || $row->status == WordsOnlineStatus::IMPORT_FAIL_STATUS
                || $row->status == "Import Failed"
                ) {
                if ($res->status != WordsOnlineConst::JOB_DELIVERED) {
                  $this->database
                    ->update(WordsOnlineConst::JOB_TABLE)
                    ->condition("job_id", $job_id)
                    ->fields(
                      [
                        "status" => WordsOnlineConst::JOB_DELIVERED,
                      ]
                    )
                    ->execute();
                }
                $edit_link = "";
                $link_text = "Import";
                if ($row->status == WordsOnlineStatus::IMPORT_FAIL_STATUS
                || $row->status == "Import Failed") {
                  $link_text = "Reimport";
                }
                $down = base_path() .
                  WordsOnlineConst::VIEW_XLF_LINK .
                  $job_id;
                $operation = $this->t(
                  "<a href=':url' class='use-ajax btn'  
                  data-dialog-type='modal' 
                  title='download'>Download</a>  <a  class='btn' onclick='reimport(@job_id
                  )'  title='@link_text'>@link_text </a>",
                  [
                    ":url" => $down,
                    "@job_id" => $job_id,
                    "@link_text" => $link_text,
                  ]
                );
              }
              break;
            }
          }
        }
        $rows[] = [
          "id" => $job_id,
          "label" => $label,
          "project_name" => $row->projectId,
          "request_id" => $row->requestId,
          "request_name" => $row->requestName,
          "lang_name" =>
          $row->sourceLanguageName . " > " . $row->targetLanguageName,
          "due_date" => $due_date,
          "status" => $status,
          "opt" => $operation,
        ];
      }
    }
    $form["searh_prefix"] = [
      "#markup" => '<div class="wol-search-field">',
    ];
    $form["searh_data"] = [
      "#type" => "textfield",
      "#name" => "job_data",
      "#value" => $key,
      "#placeholder" => WordsOnlineConst::SEARCH_PLACEHOLDER,
    ];
    $form["actions"] = [
      "#type" => "button",
      "#value" => $this->t("Search"),
      "#name" => "job-search",
      "#attributes" => [
        "class" => ["btn", "btn-layout", "ml-3", "wol-btn"],
        "onclick" => "searchJob()",
      ],
    ];
    $pageLimits = [];
    $pageLimits["10"] = "10";
    $pageLimits["15"] = "15";
    $pageLimits["20"] = "20";
    $pageLimits["25"] = "25";
    $form["page_limit"] = [
      "#type" => "select",
      "#required" => TRUE,
      "#name" => "job_page",
      "#default_value" => $top,
      "#options" => $pageLimits,
      "#attributes" => ["onchange" => "changePageLimit()"],
      '#value' => $top,
    ];
    $form["searh_surfix"] = [
      "#markup" => "</div>",
    ];
    $form["table"] = [
      "#type" => "table",
      "#header" => $header,
      "#rows" => $rows,
      "#empty" => $this->t("No records found"),
      "#attributes" => ["class" => ["wol-table"]],
    ];
    $form["pager"] = [
      "#type" => "pager",
    ];
    $pagging = "<div class='pagination'>";
    if ($pageCount > 0) {
      $pagging = $pagging . "<a href='#' class='wo-page' data-page='1'>&laquo;</a>";
      if ($skip == "" || $skip == NULL) {
        $skip = 1;
      }
      for ($i = 1; $i <= $pageCount; $i++) {
        $active = "";
        if ($i == $skip) {
          $active = "active";
        }
        $pagging =
          $pagging .
          "<a class='$active wo-page' href='#' data-page='$i'>" .
          $i .
          "</a>";
      }
      $pagging =
        $pagging .
        "<a href='#' class='wo-page' data-page='$pageCount'>&raquo;</a></div>";
    }
    $pagging = $pagging . "</div>";
    $form["info_prefix"] = [
      "#markup" => $pagging,
    ];
    $form["#attached"]["library"][] = "wordsonline_connector/wordsonline_job";
    return $form;
  }

  /**
   * Get list requests.
   *
   * @param string $filter
   *   The filter string.
   * @param int $skip
   *   Skip record.
   * @param string $top
   *   Number of record per page.
   */
  public function getDataList($filter, $skip, $top = "10") {
    $page = 0;
    if ($skip != NULL && $skip != "") {
      $page = (int) $skip;
      if ($page > 0) {
        $page = $page - 1;
      }
    }
    $page = $page * 10;
    $results = $this->database
      ->query(WordsOnlineConst::SELECT_CONFIG)
      ->fetchAssoc();
    if ($results != NULL) {
      $auth["username"] = $results["username"];
      $auth["password"] = $results["password"];
      $auth["scope"] = $results["scope"];
      $auth["grant_type"] = $results["grant_type"];
      $response = wordsonline_connector_get_token("POST", $auth);
      $response = json_decode($response, TRUE);
      $token = $response["access_token"];
      $url = WordsOnlineConst::API_URL .
        'Requests?$skip=' .
        $page .
        '&$top=' .
        $top .
        '&$orderby=CreatedAt desc ';
      if ($filter != NULL && $filter != "") {
        $searchId = "or contains(tolower(ProjectId),tolower(%27" .
          $filter .
          "%27)) ";
        if (is_numeric($filter)) {
          $searchId = " or RequestId eq  " . $filter . " ";
        }
        $url = $url .
          '&$filter=(contains(tolower(TargetLanguage),tolower(%27' .
          $filter .
          "%27)) or contains(tolower(SourceLanguage),tolower(%27" .
          $filter .
          "%27)) or contains(tolower(RequestName),tolower(%27" .
          $filter .
          "%27))  " .
          $searchId .
          ")";
      }
      $result = $this->client->get(
        $url,
        [
          "headers" => [
            "Authorization" => "Bearer " . $token,
            "Accept" => "application/json",
            "Referer" => "ClientAPI",
          ],
          "timeout" => 3600,
        ]
      );
      if ($result->getStatusCode() == 200) {
        $data = json_decode($result->getBody()->getContents());
        if ($data->status == 1) {
          return $data->result;
        }
      }
    }
    return NULL;
  }

  /**
   * Get quote info for job.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A json response.
   */
  public function getQuote(Request $request) {
    $res["status"] = "error";
    if ($request->query->get("job_id")) {
      $job_id = $request->query->get("job_id");
      $results = $this->database
        ->query(WordsOnlineConst::SELECT_CONFIG)
        ->fetchAssoc();
      if ($results != NULL) {
        $auth["username"] = $results["username"];
        $auth["password"] = $results["password"];
        $auth["scope"] = $results["scope"];
        $auth["grant_type"] = $results["grant_type"];
        $response = wordsonline_connector_get_token("POST", $auth);
        $response = json_decode($response, TRUE);
        $token = $response["access_token"];
        $req = $this->database
          ->query(
            "SELECT  * FROM wordsonline_connector_jobs 
             where job_id =:job_id LIMIT 1",
             [":job_id" => $job_id]
          )
          ->fetchAssoc();
        if ($req != NULL) {
          $quotes = wordsonline_connector_get_quote($req["request_guid"], $token);
          if ($quotes != NULL && $quotes->status == 1) {
            $res["status"] = "success";
            $res["data"] = $quotes->result;
          }
        }
        return new JsonResponse($res);
      }
      else {
        return new JsonResponse($res);
      }
    }
    else {
      return new JsonResponse($res);
    }
  }

  /**
   * Check has delivery files.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A json response.
   */
  public function checkHasFiles() {
    $ret = $this->database
      ->query(WordsOnlineConst::SELECT_CONFIG)
      ->fetchAssoc();
    if ($ret != NULL) {
      $auth["username"] = $ret["username"];
      $auth["password"] = $ret["password"];
      $auth["scope"] = $ret["scope"];
      $auth["grant_type"] = $ret["grant_type"];
      $response = wordsonline_connector_get_token("POST", $auth);
      $response = json_decode($response, TRUE);
      $token = $response["access_token"];
      $results = $this->database->select(WordsOnlineConst::JOB_TABLE, "wol");
      $results->fields("wol");
      $results->condition(
        "wol.status",
        [
          WordsOnlineConst::JOB_ORDERED,
          WordsOnlineConst::JOB_DELIVERED,
        ],
        "in"
      );
      $results->orderBy("wol.job_id", "DESC");
      $res = $results->execute()->fetchAll();
      $has_change = FALSE;
      foreach ($res as $row) {
        $files = wordsonline_connector_get_request_files_list($row->request_guid, $token);
        if ($files != NULL) {
          if ($files->result != NULL && count($files->result) > 0) {
            $job = Job::load($row->job_id);
            if ($job) {
              $auto_import = $job->getSetting("auto_import");
              $translator = $job->getTranslator();
              $translator->setSetting("xliff_processing", TRUE);
              $translator->save();
              $is_auto = $translator->isAutoAccept();
              if ($row->status == WordsOnlineConst::JOB_ORDERED) {
                $has_change = TRUE;
              }
              if ($auto_import == TRUE
                || $auto_import == 1
                || $is_auto == TRUE
                || $is_auto == 1
              ) {
                $is_import = FALSE;
                $this->database
                  ->update(WordsOnlineConst::JOB_TABLE)
                  ->condition("job_id", $row->job_id)
                  ->fields(
                    [
                      "status" => WordsOnlineConst::JOB_DELIVERED,
                    ]
                  )
                  ->execute();
                foreach ($files->result as $fi) {
                  $file_guid = $fi->guid;
                  $data = wordsonline_connector_download_file(
                    $row->request_guid,
                    $file_guid,
                    $token
                  );
                  $fName = "public://" . $fi->name;
                  file_put_contents($fName, $data);
                  $zip = new ZipHandle($fName, $this->archiver, $this->fileSystem);
                  $fileContent = $zip->fileContent;
                  $this->fileSystem->delete(
                    $fName
                  );
                  $is_has_error = FALSE;
                  if ($fileContent != NULL
                    && count($fileContent) > 0
                  ) {
                    foreach ($fileContent as $fcontent) {
                      if (!$this->importTranslation(
                        $job,
                        $fcontent
                      )) {
                        $this->messenger->addError("an error occured when import job {$row->job_id} with zip file {$fName}");
                        $this->sendErrorToWordsOnline(
                          $row->request_guid,
                          $fcontent,
                          $token
                        );
                        $is_has_error = TRUE;
                      }
                    }
                  }
                  if ($is_has_error == TRUE) {
                    continue;
                  }
                  if ($is_auto == TRUE || $is_auto == 1) {
                    wordsonline_connector_request_action(
                        $row->request_guid,
                        WordsOnlineStatus::FINISHED_STATUS,
                        "",
                        $token
                      );
                    $this->database
                      ->update(WordsOnlineConst::JOB_TABLE)
                      ->condition("job_id", $row->job_id)
                      ->fields(
                          [
                            "status" => WordsOnlineConst::JOB_FINISHED,
                          ]
                        )
                      ->execute();
                  }
                  else {
                    wordsonline_connector_request_action(
                        $row->request_guid,
                        WordsOnlineStatus::IMPORTED_STATUS,
                        "",
                        $token
                      );
                    $this->database
                      ->update(WordsOnlineConst::JOB_TABLE)
                      ->condition("job_id", $row->job_id)
                      ->fields(
                          [
                            "status" => WordsOnlineConst::JOB_IMPORTED,
                          ]
                        )
                      ->execute();
                  }
                  $is_import = TRUE;
                }
                if ($is_import) {
                  $has_change = TRUE;
                }
              }
              else {
                $this->database
                  ->update(WordsOnlineConst::JOB_TABLE)
                  ->condition("job_id", $row->job_id)
                  ->fields(
                    [
                      "status" => WordsOnlineConst::JOB_DELIVERED,
                    ]
                  )
                  ->execute();
              }
            }
          }
        }
      }
      return new JsonResponse(
        [
          "status" => "success",
          "changed" => $has_change,
        ]
      );
    }
    else {
      $this->messenger->addError(
        $this->t(
          'Cannot find WordsOnline provider settings. 
          Please contact <a href="mailto:@url">@url</a> 
          to obtain WordsOnline provider settings.',
          [
            "@url" => WordsOnlineConst::HELPDESK_MAIL,
          ]
        )
      );
      return new JsonResponse(["status" => "error", "changed" => FALSE]);
    }
  }

  /**
   * Imports the given translation data.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   The job to import translations for.
   * @param string $translation
   *   The translation data.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Throws an exception if XLF plugin does not exist.
   * @throws \Drupal\tmgmt\TMGMTException
   *   Throws an exception in case of a neeror.
   */
  public function importTranslation(JobInterface $job, $translation) {
    // $pattern = "/<bpt[^>]*>/";
    // $translation = preg_replace($pattern, "", $translation);
    // $pattern = "/<\/bpt[^>]*>/";
    // $translation = preg_replace($pattern, "", $translation);
    // $pattern = "/<ept[^>]*>/";
    // $translation = preg_replace($pattern, "", $translation);
    // $pattern = "/<\/ept[^>]*>/";
    // $translation = preg_replace($pattern, "", $translation);
    // $pattern = "/<ph [^>]*>/";
    // if (preg_match_all($pattern, $translation, $matches)) {
    //   foreach ($matches as $val) {
    //     $target = str_replace("<ph", "&lt;img", $val);
    //     $target = str_replace(">", "&gt;", $target);
    //     $translation = str_replace($val, $target, $translation);
    //   }
    // }
    // $pattern = "/<x [^>]*>/";
    // if (preg_match_all($pattern, $translation, $matches)) {
    //   foreach ($matches as $val) {
    //     $translation = str_replace($val, '&lt;br /&gt;', $translation);
    //   }
    // }
    // $pattern = "/<note>(.*?)<\/note>/";
    // if (preg_match($pattern, $translation, $matches)) {
    //   foreach ($matches as $val) {
    //     $valSource = "<note>" . $val . "</note>";
    //     $valTarget = "";
    //     $translation = str_replace($valSource, $valTarget, $translation);
    //   }
    // }
    libxml_use_internal_errors(TRUE);
    $xliff = $this->formatManager->createInstance("xlf");
    if (!$xliff->validateImport($translation, FALSE)) {
      libxml_use_internal_errors(FALSE);
      return FALSE;
    }
    if ($data = $xliff->import($translation, FALSE)) {
      $job->addTranslatedData(
        $data,
        NULL,
        TMGMT_DATA_ITEM_STATE_TRANSLATED
      );
      $job->addMessage("The translation has been received.");
    }
    else {
      libxml_use_internal_errors(FALSE);
      return FALSE;
    }
    libxml_use_internal_errors(FALSE);
    return TRUE;
  }

  /**
   * Set request fail and send mail.
   *
   * @param string $request_guid
   *   Guid of request.
   * @param string $xmlData
   *   Xml content.
   * @param string $token
   *   The token for authorization.
   */
  public function sendErrorToWordsOnline($request_guid, $xmlData, $token) {
    $messages = wordsonline_connector_get_error_message($xmlData);
    wordsonline_connector_request_action(
      $request_guid,
      WordsOnlineStatus::IMPORT_FAIL_STATUS,
      $messages,
      $token
    );
  }

  /**
   * Import file.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A json response.
   */
  public function reimport(Request $request) {
    if ($request->query->get("job_id")) {
      $job_id = $request->query->get("job_id");
      $ret = $this->database
        ->query(WordsOnlineConst::SELECT_CONFIG)
        ->fetchAssoc();
      if ($ret != NULL) {
        $auth["username"] = $ret["username"];
        $auth["password"] = $ret["password"];
        $auth["scope"] = $ret["scope"];
        $auth["grant_type"] = $ret["grant_type"];
        $response = wordsonline_connector_get_token("POST", $auth);
        $response = json_decode($response, TRUE);
        $token = $response["access_token"];
        $request = $this->database
          ->query(
            "SELECT  * FROM wordsonline_connector_jobs
              WHERE job_id =:job_id LIMIT 1",
            [":job_id" => $job_id]
          )
          ->fetchAssoc();
        if ($request != NULL) {
          $files = wordsonline_connector_get_request_files_list(
            $request["request_guid"],
            $token
          );
          if ($files != NULL) {
            if ($files->result != NULL
              && count($files->result) > 0
            ) {
              $status = "downloaded";
              $job = Job::load($job_id);
              if ($job) {
                $is_import = FALSE;
                $translator = $job->getTranslator();
                $translator->setSetting(
                  "xliff_processing",
                  TRUE
                );
                $translator->save();
                $is_auto = $translator->isAutoAccept();
                foreach ($files->result as $fi) {
                  $file_guid = $fi->guid;
                  $data = wordsonline_connector_download_file(
                    $request["request_guid"],
                    $file_guid,
                    $token
                  );
                  $fName = "public://" . $fi->name;
                  file_put_contents($fName, $data);
                  $zip = new ZipHandle($fName, $this->archiver, $this->fileSystem);
                  $fileContent = $zip->fileContent;
                  $this->fileSystem->delete(
                    $fName
                  );
                  $is_has_error = FALSE;
                  if ($fileContent != NULL
                    && count($fileContent) > 0
                  ) {
                    foreach ($fileContent as $fcontent) {
                      if (!$this->importTranslation(
                        $job,
                        $fcontent
                      )) {
                        $this->messenger->addError("an error occured when import job {$job_id} with zip file {$fName}");
                        $this->sendErrorToWordsOnline(
                          $request["request_guid"],
                          $fcontent,
                          $token
                        );
                        $is_has_error = TRUE;
                      }
                    }
                  }
                  if ($is_has_error == TRUE) {
                    continue;
                  }
                  if ($is_auto == TRUE || $is_auto == 1) {
                    wordsonline_connector_request_action(
                        $request["request_guid"],
                        WordsOnlineStatus::FINISHED_STATUS,
                        "",
                        $token
                      );
                    $this->database
                      ->update(WordsOnlineConst::JOB_TABLE)
                      ->condition("job_id", $job_id)
                      ->fields(
                          [
                            "status" => WordsOnlineConst::JOB_FINISHED,
                          ]
                        )
                      ->execute();
                  }
                  else {
                    wordsonline_connector_request_action(
                        $request["request_guid"],
                        WordsOnlineStatus::IMPORTED_STATUS,
                        "",
                        $token
                      );
                    $this->database
                      ->update(WordsOnlineConst::JOB_TABLE)
                      ->condition("job_id", $job_id)
                      ->fields(
                          [
                            "status" => WordsOnlineConst::JOB_IMPORTED,
                          ]
                        )
                      ->execute();
                  }
                  $is_import = TRUE;
                }
                if ($is_import) {
                  $status = "success";
                }
              }
              return new JsonResponse(["status" => $status]);
            }
          }
        }
      }
      else {
        $this->messenger->addError(
          $this->t(
            'Cannot find WordsOnline provider settings. 
            Please contact <a href="mailto:@url">@url</a> 
            to obtain WordsOnline provider settings.',
            [
              "@url" => WordsOnlineConst::HELPDESK_MAIL,
            ]
          )
        );
      }
    }
    return new JsonResponse(["status" => "error"]);
  }

  /**
   * Display xlf file for review.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   */
  public function viewXlf(Request $request) {
    $xmlData = [];
    $fName ="";
    if ($request->query->get("job_id")) {
      $job_id = $request->query->get("job_id");
      $ret = $this->database
        ->query(WordsOnlineConst::SELECT_CONFIG)
        ->fetchAssoc();
      if ($ret != NULL) {
        $auth["username"] = $ret["username"];
        $auth["password"] = $ret["password"];
        $auth["scope"] = $ret["scope"];
        $auth["grant_type"] = $ret["grant_type"];
        $response = wordsonline_connector_get_token("POST", $auth);
        $response = json_decode($response, TRUE);
        $token = $response["access_token"];
        $request = $this->database
          ->query(
            "SELECT  * FROM wordsonline_connector_jobs 
              WHERE job_id =:job_id LIMIT 1",
            [":job_id" => $job_id]
          )
          ->fetchAssoc();
        if ($request != NULL) {
          $files = wordsonline_connector_get_request_files_list(
            $request["request_guid"],
            $token
          );
          if ($files != NULL) {
            foreach ($files->result as $fi) {
              $file_guid = $fi->guid;
              $data = wordsonline_connector_download_file(
                $request["request_guid"],
                $file_guid,
                $token
              );
              $fName = "public://" . $fi->name;
              file_put_contents($fName, $data);
              $zip = new ZipHandle($fName, $this->archiver, $this->fileSystem);
              $fileContent = $zip->fileContent;
              $fileNames = $zip->fileNames;
              //$this->fileSystem->delete($fName);
              if ($fileContent != NULL
                && count($fileContent) > 0
              ) {
                $i = 0;
                foreach ($fileContent as $fcontent) {
                  array_push($xmlData, new WOFile(
                    $fileNames[$i],
                    $fcontent
                  ));
                  $i++;
                }
              }
            }
          }
        }
      }
      else {
        $this->messenger->addError(
          $this->t(
            'Cannot find WordsOnline provider settings. 
            Please contact <a href="mailto:@url">@url</a> 
            to obtain WordsOnline provider settings.',
            [
              "@url" => WordsOnlineConst::HELPDESK_MAIL,
            ]
          )
        );
      }
    }
    $modal_form = $this->formBuilder()->getForm(
      '\Drupal\wordsonline_connector\Form\ViewXlfForm',
      $xmlData,
      $fName
    );
    $render["form"] = $modal_form;
    return $render;
  }

  /**
   * Get xlf file content.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   */
  public function getFileContent(Request $request) {
    $result = "";
    if ($request->query->get("fileName")) {
      $fileName = $request->query->get("fileName");
      $tempPath = "temporary://wordsonline_connector/$fileName";
      if (!file_exists($tempPath)) {
        return new JsonResponse(
          [
            "status" => "error",
            "result" => "",
            "messages" => "File $fileName is not exists",
          ]
        );
      }
      $result = file_get_contents($tempPath);
    }
    else {
      return new JsonResponse(
        [
          "status" => "error",
          "result" => "",
          "messages" => "Paramerter is incorrect",
        ]
      );
    }
    return new JsonResponse(
      [
        "status" => "success",
        "result" => $result,
        "messages" => "",
      ]
    );
  }

  /**
   * Get zip file content.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   */
  public function downloadJobFile(Request $request) {
    $fileName = $request->query->get("fileName");
    $uri = 'public://' . $fileName;
    $headers = [
      'Content-Type'     => 'application/zip',
      'Content-Disposition' => 'attachment;filename="' . $fileName . '"',
    ];
    return new BinaryFileResponse($uri, 200, $headers, TRUE);
  }

}

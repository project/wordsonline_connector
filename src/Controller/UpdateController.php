<?php

namespace Drupal\wordsonline_connector\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for handle update.
 */
class UpdateController extends ControllerBase {

  /**
   * Update page render.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request from client.
   *
   * @return Drupal\Core\Render\Element
   *   Return new form.
   */
  public function index(Request $request) {
    $modal_form = $this->formBuilder()
      ->getForm('\Drupal\wordsonline_connector\Form\UpdateForm');
    $render['form'] = $modal_form;
    return $render;
  }

}
